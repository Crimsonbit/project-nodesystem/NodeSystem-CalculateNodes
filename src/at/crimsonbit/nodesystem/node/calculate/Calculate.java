package at.crimsonbit.nodesystem.node.calculate;

import at.crimsonbit.nodesystem.gui.node.GNode;
import at.crimsonbit.nodesystem.node.IGuiNodeType;
import javafx.scene.paint.Color;

/**
 * 
 * @author Florian Wagner
 *
 */
public enum Calculate implements IGuiNodeType {

	ABSOLUTE("Abs. Node"), CLAMP("Clamp Node"), NEGATE("Negate Node"), RANGEMAP("Range-Map node"), BOOL(
			"Bool Node"), EQUAL("Equal Node"), IF(
					"If-Condition Node"), IF_ELSE("If-Else-Condition Node"), IF_OBJECT("If-Object-Condiction Node");
	private String name;

	private Calculate(String s) {

		this.name = s;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public Class<? extends GNode> getCustomNodeClass() {
		return GNode.class;
	}

	@Override
	public Color getColor() {
		return Color.DARKORANGE;
	}
}
